package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class SausageCrustDough implements Dough {
    public String toString() {
        return "SausageCrust style thick crust with sausage inside";
    }
}
