package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class GourmetClams implements Clams {

    public String toString() {
        return "Gourmet Clam from our secret place";
    }
}
