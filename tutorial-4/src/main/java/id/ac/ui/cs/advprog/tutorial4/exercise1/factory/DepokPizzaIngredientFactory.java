package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.GoatCheese;
//import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.GourmetClams;
//import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.SausageCrustDough;
//import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BBQSauce;
//import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BellPepper;
//import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
//import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
//import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
//import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new SausageCrustDough();
    }

    public Sauce createSauce() {
        return new BBQSauce();
    }

    public Cheese createCheese() {
        return new GoatCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new BlackOlives(), new BellPepper()};
        return veggies;
    }

    public Clams createClam() {
        return new GourmetClams();
    }
}
