package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class CrustySandwich extends Food {
    public CrustySandwich() {
        //TODO Implement - done
        this.description = "Crusty Sandwich";
    }

    @Override
    public double cost() {
        //TODO Implement - done
        return 1.0;
    }
}
