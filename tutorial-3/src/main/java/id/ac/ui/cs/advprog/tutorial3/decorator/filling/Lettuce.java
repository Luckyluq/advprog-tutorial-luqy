package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Lettuce extends Food {
    Food food;

    public Lettuce(Food food) {
        //TODO Implement - done
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement - done
        return food.getDescription() + ", adding lettuce";
    }

    @Override
    public double cost() {
        //TODO Implement - done
        return food.cost() + 0.75;
    }
}
