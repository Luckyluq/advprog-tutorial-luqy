package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Food {
    Food food;

    public ChickenMeat(Food food) {
        //TODO Implement - done
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement - done
        return food.getDescription() + ", adding chicken meat";
    }

    @Override
    public double cost() {
        //TODO Implement - done
        return food.cost() + 4.5;
    }
}
